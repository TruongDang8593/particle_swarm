program particle_swarm
  use m_config
  use m_particle_core
  use m_particle_swarm
  use m_io

  implicit none

  integer, parameter  :: dp = kind(0.0d0)
  type(CFG_t)         :: cfg
  character(len=80)   :: swarm_name
  integer             :: swarm_size
  type(PC_t)          :: pc    ! Particle data
  type(SWARM_field_t) :: field ! The field configuration
  type(SWARM_td_t)    :: td(SWARM_num_td)
  logical             :: dry_run

  call initialize_all(cfg)
  call CFG_get(cfg, "dry_run", dry_run)

  if (.not. dry_run) then
     call get_field_configuration(cfg, field)
     call CFG_get(cfg, "swarm_size", swarm_size)

     call SWARM_initialize(cfg, td, field)
     call SWARM_get_data(pc, swarm_size, td)
     call SWARM_print_results(td)
  end if

contains

  !> Initializes everything needed for the simulation
  subroutine initialize_all(cfg)
    use m_gas
    use m_cross_sec
    use m_units_constants
    type(CFG_t), intent(inout)     :: cfg
    integer                        :: nn, tbl_size, max_num_part
    integer                        :: swarm_size, n_gas_comp, n_gas_frac
    real(dp)                       :: pressure, temperature, max_ev
    real(dp)                       :: magnetic_field, electric_field, tmp
    character(len=200)             :: cs_file, output_dir
    character(len=200)             :: cfg_name, prev_name, tmp_name
    character(len=20)              :: particle_mover
    character(len=20), allocatable :: gas_names(:)
    real(dp), allocatable          :: gas_fracs(:)
    type(CS_t), allocatable        :: cross_secs(:)
    logical                        :: consecutive_run
    logical                        :: dry_run

    ! Create default parameters for the simulation (routine contained below)
    call create_sim_config(cfg)
    call CFG_sort(cfg)

    swarm_name = ""
    prev_name = ""
    do nn = 1, command_argument_count()
       call get_command_argument(nn, cfg_name)
       call CFG_read_file(cfg, trim(cfg_name))

       call CFG_get(cfg, "swarm_name", tmp_name)
       if (tmp_name /= "" .and. tmp_name /= prev_name) then
          if (swarm_name /= "") then
             swarm_name = trim(swarm_name) // "_" // trim(tmp_name)
          else
             swarm_name = trim(tmp_name)
          end if
       end if
       prev_name = tmp_name
    end do

    call CFG_get_size(cfg, "gas_components", n_gas_comp)
    call CFG_get_size(cfg, "gas_fractions", n_gas_frac)
    if (n_gas_comp /= n_gas_frac) &
         print *, "gas_components and gas_component_fracs have unequal size"
    allocate(gas_names(n_gas_comp))
    allocate(gas_fracs(n_gas_comp))

    call CFG_get(cfg, "gas_components", gas_names)
    call CFG_get(cfg, "gas_fractions", gas_fracs)
    call CFG_get(cfg, "gas_temperature", temperature)
    call CFG_get(cfg, "gas_pressure", pressure)

    ! Initialize gas and electric field module
    call GAS_initialize(gas_names, gas_fracs, pressure, temperature)

    call CFG_get(cfg, "consecutive_run", consecutive_run)
    call CFG_get(cfg, "output_dir", output_dir)

    if (.not. consecutive_run) then
       tmp_name = trim(output_dir) // "/" // trim(swarm_name) // "_config.txt"
       print *, "Writing configuration to ", trim(tmp_name)
       call CFG_write(cfg, trim(tmp_name))

       call CFG_get(cfg, "gas_file", cs_file)
       call CFG_get(cfg, "particle_max_energy_ev", max_ev)

       do nn = 1, n_gas_comp
          call CS_add_from_file(trim(cs_file), &
               trim(gas_names(nn)), gas_fracs(nn) * &
               GAS_number_dens, max_ev, cross_secs)
       end do

       call CS_write_summary(cross_secs, &
            trim(output_dir) // "/" // trim(swarm_name) // "_cs_summary.txt")

       print *, "Initializing particle model", 1
       call CFG_get(cfg, "particle_lkptbl_size", tbl_size)
       call CFG_get(cfg, "swarm_size", swarm_size)

       ! Allocate storage for 8 times the swarm size. There are checks in place
       ! to make sure it cannot grow to such a large size.
       max_num_part = 8 * swarm_size

       call pc%initialize(UC_elec_mass, cross_secs, &
            tbl_size, max_ev, max_num_part, get_random_seed())

       print *, "--------------------"
       print *, "Gas information"
       write(*, fmt="(A10,A3,E9.3)") "Temp. (K)", " - ", temperature
       print *, ""
       print *, "Component - fraction"
       do nn = 1, n_gas_comp
          write(*, fmt="(A10,A3,E9.3)") trim(gas_names(nn)), " - ", &
               gas_fracs(nn)
       end do
       print *, ""

       tmp_name = trim(output_dir) // "/" // trim(swarm_name) // "_coeffs.txt"
       print *, "Writing colrate table (as text) to ", trim(tmp_name)
       call IO_write_coeffs(pc, trim(tmp_name))

       tmp_name = trim(output_dir) // "/" // trim(swarm_name)
       print *, "Writing particle params (raw) to ", &
            trim(tmp_name) // "_params.dat"
       print *, "Writing particle lookup table (raw) to ", &
            trim(tmp_name) // "_lt.dat"
       call pc%to_file(trim(tmp_name) // "_params.dat", &
            trim(tmp_name) // "_lt.dat")
       print *, "--------------------"

    else ! Restarted run (can only change field!)
       tmp_name = trim(output_dir) // "/" // trim(swarm_name)
       call pc%init_from_file(trim(tmp_name) // "_params.dat", &
            trim(tmp_name) // "_lt.dat", get_random_seed())
    end if

    call CFG_get(cfg, "particle_mover", particle_mover)
    call CFG_get(cfg, "magnetic_field", magnetic_field)
    call CFG_get(cfg, "electric_field", electric_field)
    call CFG_get(cfg, "dry_run", dry_run)

    select case (trim(particle_mover))
    case ("analytic")
       if (electric_field > 10.0_dp * magnetic_field * UC_lightspeed) then
          ! Negligible B-field, use simplified approximation
          pc%particle_mover => SWARM_particle_mover_simple
       else
          pc%particle_mover => SWARM_particle_mover_analytic
       end if
    case ("boris")
       pc%particle_mover => SWARM_particle_mover_boris
       ! Limit time step to boris_dt_factor / cyclotron freq.
       call CFG_get(cfg, "boris_dt_factor", tmp)
       pc%dt_max = tmp * 2 * UC_pi / abs(magnetic_field * UC_elec_q_over_m)
    case ("verlet")
       pc%particle_mover => PC_verlet_advance
    case default
       stop "Incorrect particle mover selected"
    end select

  end subroutine initialize_all

  !> Create the parameters and their default values for the simulation
  subroutine create_sim_config(cfg)
    type(CFG_t), intent(inout) :: cfg

    call CFG_add(cfg, "consecutive_run", .false., &
         "True means: use data from a previous run with the same name")
    call CFG_add(cfg, "dry_run", .false., &
         "True means: only write simulation settings, no results'")

    ! General simulation parameters
    call CFG_add(cfg, "electric_field", 1.0e7_dp, &
         "The electric field")
    call CFG_add(cfg, "magnetic_field", 0.0_dp, &
         "The electric field")
    call CFG_add(cfg, "field_angle_degrees", 90.0_dp, &
         "The angle between the electric and magnetic field")
    call CFG_add(cfg, "swarm_name", "my_sim", &
         "The name of the swarm simulation")
    call CFG_add(cfg, "swarm_size", 1000, &
         "The initial size of a swarm")
    call CFG_add(cfg, "output_dir", "output", &
         "The output directory (include no trailing slash!)")

    call CFG_add(cfg, "acc_velocity_sq", [5.0e-3_dp, 0.0_dp], &
         "The required rel/abs accuracy of the velocity squared")
    call CFG_add(cfg, "acc_velocity", [5.0e-3_dp, 0.0_dp], &
         "The required rel/abs accuracy of the velocity")
    call CFG_add(cfg, "acc_diffusion", [1.0e-2_dp, 0.0_dp], &
         "The required rel/abs accuracy of the diffusion coeff.")
    call CFG_add(cfg, "acc_alpha", [5.0e-3_dp, 0.0_dp], &
         "The required rel/abs accuracy of the ionization coeff.")

    ! Gas parameters
    call CFG_add(cfg, "gas_pressure", 1.0_dp, &
         "The gas pressure (bar)")
    call CFG_add(cfg, "gas_temperature", 300.0_dp, &
         "The gas temperature (Kelvin)")
    call CFG_add(cfg, "gas_components", (/"N2"/), &
         "The names of the gases used in the simulation", .true.)
    call CFG_add(cfg, "gas_file", "input/cross_sections_nitrogen.txt", &
         "The file in which to find cross section data")
    call CFG_add(cfg, "gas_fractions", (/1.0_dp /), &
         & "The partial pressure of the gases (as if they were ideal gases)", .true.)

    ! Particle model related parameters
    call CFG_add(cfg, "particle_lkptbl_size", 10000, &
         "The size of the lookup table for the collision rates")
    call CFG_add(cfg, "particle_max_energy_ev", 500.0_dp, &
         "The maximum energy in eV for particles in the simulation")
    call CFG_add(cfg, "particle_mover", "verlet", &
         "Which particle mover to use. Options: analytic, verlet, boris")
    call CFG_add(cfg, "boris_dt_factor", 0.1_dp, &
         "The maximum time step in terms of the cylotron frequency")
  end subroutine create_sim_config

  function get_random_seed() result(seed)
    integer :: seed(4)
    integer :: time, i

    call system_clock(time)
    do i = 1, 4
       seed(i) = ishftc(time, i*8)
    end do
  end function get_random_seed

  subroutine get_field_configuration(cfg, field)
    use m_units_constants
    type(CFG_t), intent(in)            :: cfg
    type(SWARM_field_t), intent(inout) :: field
    real(dp)                           :: electric_field, degrees

    ! Set electric and magnetic fields
    call CFG_get(cfg, "magnetic_field", field%Bz)
    call CFG_get(cfg, "electric_field", electric_field)
    call CFG_get(cfg, "field_angle_degrees", degrees)

    field%angle_deg = degrees
    field%Ez        = electric_field * cos(UC_pi * degrees / 180)
    field%Ey        = electric_field * sin(UC_pi * degrees / 180)

    field%B_vec = [0.0_dp, 0.0_dp, field%Bz]
    field%E_vec = [0.0_dp, field%Ey, field%Ez]

    field%omega_c   = -UC_elec_charge * field%Bz / UC_elec_mass
    field%ExB_drift = [field%Ey * field%Bz, 0.0_dp, 0.0_dp] / &
         max(epsilon(1.0_dp), field%Bz**2)

  end subroutine get_field_configuration

end program particle_swarm
